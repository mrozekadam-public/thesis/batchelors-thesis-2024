using Microsoft.EntityFrameworkCore;

namespace Newton.Thesis.Infrastructure.Repositories.Base;

public interface IBaseRepository<C> : IRepository where C : DbContext
{
    IDbContextFactory<C> DbContextFactory { get; set; }
    C DbContext { get; set; }
    Task<T> GetByIdAsync<T>(int id) where T : class;
    Task<T> GetById<T, K>(K id) where T : class;
    Task Add<T>(T entity) where T : class;
    void Update<T>(T entity) where T : class;
    void Remove<T>(T entity) where T : class;
    void RemoveRange<T>(IEnumerable<T> entities) where T : class;
}


public abstract class BaseRepository<C> : IBaseRepository<C> where C : DbContext
{
    private C dbContext;
    public C DbContext
    {
        get => dbContext;
        set
        {
            dbContext?.Dispose(); // dispose any current context
            dbContext = value;
        }
    }
    public IDbContextFactory<C> DbContextFactory { get; set; }
    
    public void Dispose()
    {

    }
    
    public T GetById<T>(int id) where T : class
    {
        return this.DbContext.Set<T>().Find(id);
    }
    
    public async Task<T> GetByIdAsync<T>(int id) where T : class
    {
        return await this.DbContext.Set<T>().FindAsync(id);
    }
    
    public async Task<T> GetById<T, K>(K id) where T : class
    {
        var entity = await DbContext.Set<T>().FindAsync(id);
        DbContext.Entry(entity).State = EntityState.Detached;
        return entity;
    }

    public async Task Add<T>(T entity) where T : class
    {
        await DbContext.Set<T>().AddAsync(entity);
    }
    
    public void Update<T>(T entity) where T : class
    {
        this.DbContext.Set<T>().Update(entity);
    }
    
    public void Remove<T>(T entity) where T : class
    {
        this.DbContext.Set<T>().Remove(entity);
    }
    
    public void RemoveRange<T>(IEnumerable<T> entities) where T : class
    {
        this.DbContext.Set<T>().RemoveRange(entities);
    }
    
    /// <summary>
    /// Get all entities of type T without tracking.
    /// Info: The AsNoTracking() method returns a new query where the change tracker will not track any of the entities that are returned.
    /// If the entity instances are modified, this will not be detected by the change tracker, and SaveChanges() will not persist those changes to the database.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    protected IQueryable<T> GetEntities<T>() where T : class
    {
        return DbContext.Set<T>().AsNoTracking();
    }

    /// <summary>
    /// Get all entities of type T with tracking. Modifications will be detected by the change tracker.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    protected DbSet<T> GetEntitiesTracked<T>() where T : class
    {
        return DbContext.Set<T>();
    }
}

public abstract class BaseRepository<T, C> : BaseRepository<C> where T : class where C : DbContext
{
    /// <summary>
    /// Return tracked entities of type T with tracking.
    /// </summary>
    public DbSet<T> EntitiesTracked => GetEntitiesTracked<T>();

    
    /// <summary>
    /// Return entities of type T without tracking.
    /// </summary>
    public IQueryable<T> Entities => GetEntities<T>();
}