using System.Linq.Expressions;
using Nest;

namespace Newton.Thesis.Infrastructure.Utils.ElasticSearch;

public class ElasticQueryFactory<TDocument>
    where TDocument : class
{
    /// <summary>
    /// Define the first item that Elastic should search from
    /// </summary>
    private int SearchFrom { get; set; } = 0;

    /// <summary>
    /// Define the number of results that should be returned.
    /// </summary>
    private int SearchSize { get; set; } = 10;

    /// <summary>
    /// List of queries used for searching.
    /// </summary>
    private List<QueryContainer> Queries { get; set; } = new List<QueryContainer>();

    /// <summary>
    /// Initialize the factory - get a
    /// </summary>
    /// <returns></returns>
    public static ElasticQueryFactory<TDocument> Initialize()
    {
        return new ElasticQueryFactory<TDocument>();
    }

    private string ConvertStringToElasticSearchColumnVariant(string input)
    {
        return System.Text.Json.JsonNamingPolicy.CamelCase.ConvertName(input);
    }

    private string GetFieldNameFromExpression<TValue>(Expression<Func<TDocument, TValue>> selector)
    {
        var memberExpression = (MemberExpression)selector.Body;
        var fieldName = memberExpression.Member.Name;

        return ConvertStringToElasticSearchColumnVariant(fieldName);
    }

    /// <summary>
    /// Add a new searchQuery
    /// </summary>
    /// <param name="selector">A lambda expression selecting the field to search on</param>
    /// <param name="searchValue">The value to search for</param>
    /// <returns>The current instance of ElasticQueryFactory for method chaining</returns>
    public ElasticQueryFactory<TDocument> TermQuery<TValue>(
        Expression<Func<TDocument, TValue>> selector,
        string searchValue
    )
    {
        // Extract the field name from the expression
        var fieldName = GetFieldNameFromExpression(selector);

        // Create and add the TermQuery
        this.Queries.Add(new TermQuery() { Field = fieldName, Value = searchValue });

        return this;
    }

    public ElasticQueryFactory<TDocument> MatchQuery<TValue>(
        Expression<Func<TDocument, TValue>> selector,
        string searchValue
    )
    {
        // Extract the field name from the expression
        var fieldName = GetFieldNameFromExpression(selector);

        // Create and add the TermQuery
        this.Queries.Add(new MatchPhrasePrefixQuery() { Field = fieldName, Query = searchValue, });

        return this;
    }

    public ElasticQueryFactory<TDocument> WildcardQuery<TValue>(
        Expression<Func<TDocument, TValue>> selector,
        string searchValue
    )
    {
        // Extract the field name from the expression
        var fieldName = GetFieldNameFromExpression(selector);

        // Create and add the TermQuery
        this.Queries.Add(new WildcardQuery() { Field = fieldName, Value = searchValue });

        return this;
    }

    /// <summary>
    /// Build the queries.
    /// </summary>
    /// <returns></returns>
    public ElasticSearchQuery Build()
    {
        return new ElasticSearchQuery()
        {
            From = this.SearchFrom,
            Size = this.SearchSize,
            Queries = this.Queries
        };
    }
}

public class ElasticSearchQuery
{
    public int From { get; set; }
    public int Size { get; set; }
    public List<QueryContainer> Queries { get; set; }
}
