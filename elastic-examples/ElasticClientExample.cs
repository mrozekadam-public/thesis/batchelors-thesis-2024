using Elasticsearch.Net;
using Microsoft.Extensions.Configuration;
using Nest;

namespace Newton.Thesis.Infrastructure.Utils.ElasticSearch;

public interface IPlatformElasticClient
{
    Task<string> IndexDocument<T>(string index, T document)
        where T : class;
    Task<bool> DeleteDocument(string index, string id);
    Task<T> GetDocumentById<T>(string index, string id)
        where T : class;

    /// <summary>
    /// Async search function based on a set of TermQueries and array sizes.
    /// </summary>
    /// <param name="index"></param>
    /// <param name="searchQuery"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    Task<List<T>> SearchAsync<T>(string index, ElasticSearchQuery searchQuery)
        where T : class;
}

public class PlatformElasticClient : IPlatformElasticClient
{
    private readonly IElasticClient _client;
    private readonly IConfiguration _configuration;

    public PlatformElasticClient(IConfiguration configuration)
    {
        this._configuration = configuration;

        var url = _configuration.GetSection("ElasticSearch:Url").Value;
        var apiKey = _configuration.GetSection("ElasticSearch:Key").Value;

        if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(apiKey))
            throw new Exception("ElasticSearch url or key not found in configuration");

        var connectionSettings = new ConnectionSettings(new Uri(url))
            .ApiKeyAuthentication(new ApiKeyAuthenticationCredentials(apiKey))
            .EnableDebugMode();

        this._client = new ElasticClient(connectionSettings);
    }

    /// <summary>
    /// Add a document to the index
    /// </summary>
    /// <param name="index"></param>
    /// <param name="document"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns>Elastic Id assigned to the document</returns>
    public async Task<string> IndexDocument<T>(string index, T document)
        where T : class
    {
        var response = await _client.IndexAsync(document, i => i.Index(index));

        if (response.IsValid)
        {
            return response.Id;
        }

        throw new Exception($"Error indexing document to index {index}");
    }

    /// <summary>
    /// Delete a document from the index
    /// </summary>
    /// <param name="index"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    public async Task<bool> DeleteDocument(string index, string id)
    {
        try
        {
            await _client.DeleteAsync(new DeleteRequest(index, id));
            return true;
        }
        catch (Exception e)
        {
            throw new Exception($"Error deleting document with id {id} from index {index}", e);
            return false;
        }
    }

    /// <summary>
    /// Get a document from the index
    /// </summary>
    /// <param name="index"></param>
    /// <param name="id"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public async Task<T> GetDocumentById<T>(string index, string id)
        where T : class
    {
        var response = await _client.GetAsync<T>(new GetRequest(index, id));

        if (response.IsValid)
        {
            return response.Source;
        }

        return null;
    }

    /// <summary>
    /// Async search function based on a set of TermQueries and array sizes.
    /// </summary>
    /// <param name="index"></param>
    /// <param name="searchQuery"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public async Task<List<T>> SearchAsync<T>(string index, ElasticSearchQuery searchQuery)
        where T : class
    {
        // Assuming searchQuery.Queries is of type List<QueryContainer> or similar
        var boolQuery = new BoolQuery { Must = searchQuery.Queries };

        var request = new SearchRequest<T>(index)
        {
            From = searchQuery.From,
            Size = searchQuery.Size,
            Query = boolQuery
        };

        var response = await _client.SearchAsync<T>(request);

        if (response.IsValid)
        {
            return response.Documents.ToList();
        }

        return new List<T>();
    }
}
