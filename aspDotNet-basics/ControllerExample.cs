using Microsoft.AspNetCore.Mvc;
using Newton.Thesis.Infrastructure.Dto.Enums;
using Newton.Thesis.Infrastructure.Operations;
using Newton.Thesis.Web.Controllers.Base;

namespace Newton.Thesis.Web.Controllers;

[Route("/api/autocomplete/")]
public class AutocompleteController : BaseController
{
    private readonly IAutocompleteEntityOperation _autocompleteEntityOperation;

    public AutocompleteController(IAutocompleteEntityOperation autocompleteEntityOperation)
    {
        this._autocompleteEntityOperation = autocompleteEntityOperation;
    }

    [HttpPost("add")]
    public async Task<IActionResult> AddEntity(
        Infrastructure.Dto.Entities.AutocompleteEntity entity
    )
    {
        return Ok(await this._autocompleteEntityOperation.AddAutocompleteEntityAsync(entity));
    }

    [HttpPost("add/bulk")]
    public async Task<IActionResult> AddEntitiesBulk(
        IEnumerable<Infrastructure.Dto.Entities.AutocompleteEntity> entities
    )
    {
        foreach (var entity in entities)
        {
            await this._autocompleteEntityOperation.AddAutocompleteEntityAsync(entity);
        }

        return Ok($"Successfully imported {entities.Count()} entities");
    }

    [HttpPost("add/{entityValue}/{entityType}")]
    public async Task<IActionResult> AddEntity(
        string entityValue,
        Infrastructure.Dto.Enums.AutocompleteEntityType entityType
    )
    {
        return Ok(
            await this._autocompleteEntityOperation.AddAutocompleteEntityAsync(
                entityValue,
                entityType
            )
        );
    }

    [HttpPost("search/{entityType}")]
    public async Task<IActionResult> SearchEntities(
        string[] searchTerms,
        Infrastructure.Dto.Enums.AutocompleteEntityType entityType
    )
    {
        return Ok(
            await this._autocompleteEntityOperation.SearchAutocompleteEntitiesAsync(
                searchTerms,
                entityType
            )
        );
    }
    
    [HttpGet("search/{searchTerm}")]
    public async Task<IActionResult> SearchEntities(
        string searchTerm
    )
    {
        return Ok(
            await this._autocompleteEntityOperation.SearchAutocompleteEntitiesAsync(
                new string[] { searchTerm },
                AutocompleteEntityType.Client
            )
        );
    }
}
