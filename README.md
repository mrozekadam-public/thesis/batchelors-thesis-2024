# Bakalářská práce 2024: Kolekce ukázkových kódů

## Úvod
Tento repositář je přílohou k mé bakalářské práci na téma "Analýza a návrh informačního systému pro společnost NEWTON Media a.s." Zde naleznete kolekci ukázkových kódů, které demonstrují klíčové koncepty a technologie použité v rámci mé práce. Zdrojový kód celého systému není veřejně dostupný. Zdrojové kódy zveřejněné zde jsou volně k použití na vlastní riziko.

# Abstrakt (cs)
Předmětem této práce je představit čtenářům proces vývoje softwaru od úvodní analýzy (sbě-ru požadavků) až po implementaci a verifikaci. Práce popisuje většinu fází SDLC cyklu na reálném projektu tvorby nového informačního systému pro českou pobočku společnosti NEWTON Media.

Hlavním cílem práce je zanalyzovat a definovat stávající komunikační problém mezi odděle-ními klientské podpory a výroby dat a tento problém dále vyřešit implementováním nového informačního systému dle potřeb podniku. Nový informační sytém by měl přispět ke zkva-litnění přenosu informací mezi výše zmíněnými odděleními. 

V úvodu práce je analyzována řešená společnost a představeny jsou dvě klíčové oblasti říze-ní, klientská podpora a výroba dat. Práce dále pokračuje sběrem uživatelských požadavků za pomocí řízených rozhovorů dle předpřipraveného scénáře.
Hlavní část práce je značně technicky orientovaná. Čtenáři jsou představeny klíčové koncep-ty z oblasti vývoje softwaru, které se přímo vývoje v rámci práce týkají. Mimo klíčových kon-ceptů jsou čtenáři představeny i využité technologie, těmi jsou například Elastic search, .NET nebo Vuejs.

Implementační kapitola dále čtenáři přiblíží různá nastavení vývojového prostředí a konfigu-raci systému Elastic za pomocí služby Docker.

V posledních částech je prototyp informačního systému představen zvoleným zaměstnan-cům a s nimi validován. Práce na závěr ještě představuje možnosti budoucího rozšíření sys-tému.

# Klíčová slova
informační systém, SDLC, .NET, Vuejs, Elastic search, Micrososft SQL Server




# Abstract (en)
The subject of this paper is to introduce the process of software development from initial analysis (requirement gathering) to implementation and verification. The thesis describes most of the phases of the SDLC cycle on a real project of creating a new information system for the Czech branch of NEWTON Media company.

The main objective of the thesis is to analyse and define the existing communication pro-blem between the client support and data production departments and to further solve this problem by implementing a new information system according to the needs of the company. The new information system should help to improve the quality of data transfer between the departments mentioned above.

In the introduction part of the thesis, the company in question is analysed, and two key ma-nagement areas, customer support and data production, are presented. The thesis then deals with collecting and analysing user requirements using guided in-person interviews and a pre-prepared scenario.

The central part of the thesis is primarily technical. The reader is introduced to key concepts in software development that are directly relevant to the development within the thesis. In addition to the fundamental concepts, the reader is also introduced to the technologies used, including Elastic search, .NET, and Vuejs.

The implementation chapter further introduces the reader to the different settings of the de-velopment environment and the configuration of Elastic using Docker.

In the last sections, the prototype of the information system is presented to the selected em-ployees and validated with them. The thesis concludes by further presenting possibilities for future extensions of the system.


# Keywords
information system, SDLC, .NET, Vuejs, Elastic search, Micrososft SQL Server

